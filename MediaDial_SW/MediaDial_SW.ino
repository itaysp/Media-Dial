#include <Adafruit_NeoPixel.h>
// #include <DigiMouse.h> //(I can't use both keyboard and mouse, in this example i'm using the keyboard)
#include <DigiKeyboard.h>

//GPIO Settings
#define GPIO_BTN 0
#define GPIO_LEDS 5
#define GPIO_ENCODER_A 1
#define GPIO_ENCODER_B 2

//Settings
#define NUM_OF_PIXELS	16	//Number of leds in the led ring
#define DECOUNCE_DELAY 50	//time between checks for button state change
#define SCOLL_JUMPS 5		//When using the dial for scroling, this is the number of regular mouse course jumps.

//Global Variables
uint8_t encoder_counter = 0;	//Stores the position of the current led in the led ring
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUM_OF_PIXELS, GPIO_LEDS, NEO_RGB + NEO_KHZ800);

//Misc
enum ENCODER_DIR { DIR_C, DIR_CC, DIR_UNCHANGED };

//Function Prototypes
ENCODER_DIR handleEncoder();
uint8_t handleButton();
void clickAction();
void doubleClickAction();
void scrollUpAction();
void scrollDownAction();

void setup() {
	//Init the GPIOs
	pinMode(GPIO_BTN, INPUT_PULLUP);
	pinMode(GPIO_ENCODER_A, INPUT_PULLUP);
	pinMode(GPIO_ENCODER_B, INPUT_PULLUP);
	pinMode(GPIO_LEDS, OUTPUT);

	//DigiMouse.begin(); // call init to enumerate (I are not using the mouse in this example)

	//Init the led ring
	pixels.begin();
	pixels.clear();
	pixels.setPixelColor(encoder_counter, pixels.Color(0, 0, 255));  //Set first pixel to blue
	pixels.show();
}


void loop() {

	switch (handleButton())
	{
	case 1: // Pressed
		//Single Press
			pixels.clear();
			pixels.setPixelColor(0, pixels.Color(255, 0, 0));
			pixels.setPixelColor(3, pixels.Color(255, 0, 0));
			pixels.setPixelColor(7, pixels.Color(255, 0, 0));
			pixels.setPixelColor(11, pixels.Color(255, 0, 0));
			pixels.show();

			//Send defined key
			clickAction();
		break;
	case 2: //Double Press
			pixels.clear();
			pixels.setPixelColor(0, pixels.Color(0, 255, 0));
			pixels.setPixelColor(3, pixels.Color(0, 255, 0));
			pixels.setPixelColor(7, pixels.Color(0, 255, 0));
			pixels.setPixelColor(11, pixels.Color(0, 255, 0));
			pixels.show();

			//Send defined key
			doubleClickAction();
		break;

	case 0: // Not Pressed
			//remove the 'button pressed' colors
			pixels.clear();		

			//show encoder position again
			pixels.setPixelColor((encoder_counter % NUM_OF_PIXELS), pixels.Color(0, 0, 255));  //Set first pixel to blue
			pixels.show();

		break;
	default:
		break;
	}

	switch (handleEncoder()) {
	case DIR_C:
		pixels.setPixelColor((encoder_counter % NUM_OF_PIXELS), pixels.Color(0, 0, 0));
		encoder_counter++;
		pixels.setPixelColor((encoder_counter % NUM_OF_PIXELS), pixels.Color(0, 0, 255));
		pixels.show();

		//Scroll up action
		scrollUpAction();
		break;

	case DIR_CC:
		pixels.setPixelColor((encoder_counter % NUM_OF_PIXELS), pixels.Color(0, 0, 0));
		encoder_counter--;
		pixels.setPixelColor((encoder_counter % NUM_OF_PIXELS), pixels.Color(0, 0, 255));
		pixels.show();

		//Scroll down action
		scrollDownAction();
		break;

	case DIR_UNCHANGED:
	default:
		break;

	}
}

ENCODER_DIR handleEncoder() {
	static uint8_t lastStateA = LOW;
	enum ENCODER_DIR dir = DIR_UNCHANGED;
	static uint32_t lastCheck = millis();

	if((millis() - lastCheck) > 5){ //200Hz Sample Rate
		uint8_t currentStateA = digitalRead(GPIO_ENCODER_A);

		if ((lastStateA == LOW) && (currentStateA == HIGH)) {
			if (digitalRead(GPIO_ENCODER_B) == LOW) {
				dir = DIR_C;
			}
			else{
				dir = DIR_CC;
			}
		}
		lastStateA = currentStateA;
		lastCheck = millis();
	}

	return dir;	
}



 uint8_t handleButton() {
	static uint8_t lastState = HIGH;
	static uint32_t lastDebounceTime = millis();
	static bool waitingForDeboung = false;
	static uint32_t lastPressTime = millis();
	static uint8_t buttonPressed = 0;

	uint8_t currentState = digitalRead(GPIO_BTN);

	//Check for button change state
	if ((currentState != lastState) && waitingForDeboung == false) {
		lastDebounceTime = millis();
		waitingForDeboung = true; //change found, now wait for the decouncing to pass
	}

	//if debouncing time passed..
	if ((millis() - lastDebounceTime) > DECOUNCE_DELAY) {
		if (currentState != lastState) { //if the state is still changed - I can confirm a button press or release
			lastState = currentState;

			if (currentState == HIGH) {		//button released	
				return 0;
			}
			else
			{
				//if this is the first press, start taking time
				if (buttonPressed == 0){
					lastPressTime = millis();
				}

				//incease the number of clicks
				buttonPressed++;
			}
		}
		waitingForDeboung = false;
	}

	//Count how many clicks were during 300ms
	if (((millis() - lastPressTime) > 300) && buttonPressed)
	{
		uint8_t tempPressVal = buttonPressed;
		buttonPressed = 0;
		lastState = LOW;
		return tempPressVal;
	}

	//if the button is not pressed in 175ms, I ignore the check for double click
	if (((millis() - lastPressTime) > 175) && (lastState == LOW) && buttonPressed == 1)
	{
		buttonPressed = 0;
		return 1;
	}

	return 3;
}

 void clickAction() {
	 // Send keyboard ',' (key code 55)
	 // Lightroom cycle through Basic panel settings (Up)
	 DigiKeyboard.sendKeyStroke(55);
 }

 void doubleClickAction() {
	 // Send keyboard '.' (key code 54)
	 // Lightroom cycle through Basic panel settings (Down)
	 DigiKeyboard.sendKeyStroke(54);
 }

 void scrollUpAction() {
/*
	// Mouse Scroll Up
	DigiMouse.scroll(SCOLL_JUMPS);
	DigiMouse.delay(50);
*/

	 // Send keyboard '+' (key code 87)
	 // Lightroom increase selected slider in small increments
	 DigiKeyboard.sendKeyStroke(87);
 }

 void scrollDownAction() {
 /*
	 //Mouse Scroll Down
	 DigiMouse.scroll((-1)*SCOLL_JUMPS);
	 DigiMouse.delay(50);
*/

	 //Send Keyboard '-' (key code 86)
	 //Lightroom decrease selected slider in small increments 
	 DigiKeyboard.sendKeyStroke(86);
 }